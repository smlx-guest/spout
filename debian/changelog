spout (1.4-4) unstable; urgency=low

  * Add patch to put exit instructions on scroll screen (Closes: #873688)
  * Bump debhelper compat version
  * Upgrade copyright URL to HTTPS
  * Bump standards-version
  * Update homepage URL to use HTTPS
  * Update watch to use HTTPS
  * Add comment to .desktop file
  * Update VCS URLs

 -- Scott Leggett <scott@sl.id.au>  Fri, 02 Nov 2018 20:31:12 +1100

spout (1.4-3) unstable; urgency=low

  * Bump Standards-Version from 3.9.4 to 3.9.7.
  * Update package VCS URLs.
  * Enable all hardening build options.
  * Remove Debian menu file in favour of .desktop file. (Closes: #738032)
  * Add patch to avoid hard-coded library search path. (Closes: #723549)
  * Add patch to remove redundant out-of-range comparison.
    Thanks to Arthur Marble for the patch. (Closes: #812249)

 -- Scott Leggett <scott@sl.id.au>  Thu, 28 Apr 2016 12:07:49 +1000

spout (1.4-2) unstable; urgency=low

  * Remove unnecessary explicit dpkg Pre-Depends.
  * Remove unnecessary explicit hardening flags and dependency.
  * Replaced hardening patch to take advantage of debhelper.
  * Add 'Keywords' field to .desktop file.
  * Upload to unstable.

 -- Scott Leggett <scott@sl.id.au>  Wed, 21 Aug 2013 22:11:01 +1000

spout (1.4-1) experimental; urgency=low

  * New Debian maintainer. (Closes: #691830)
  * Imported new upstream version 1.4.
  * New upstream maintainer and homepage.
  * Fix typo and other formatting issues in .desktop file. (Closes: #556938)
  * Apply 64-bit segfault patch to upstream.
  * Merge new upstream and old Debian man pages.
  * Build-depend on hardening-wrapper.
  * Patched hardening errors.
  * Updated packaging to use debhelper, quilt 3.0.
  * Bump standards version to 3.9.4.
  * Bump compat version to 9.
  * Add new packaging VCS information.

 -- Scott Leggett <scott@sl.id.au>  Mon, 25 Feb 2013 23:23:58 +1100

spout (1.3-2) unstable; urgency=low

  * Fix segfault on 64 bit builds. (Closes: #525722)
    Many thanks to Josh Tripplet and Steve Cotton.
    (sed -i "s,unsigned long,uint32_t,g" spout.c)
  * Bump standards version.
  * Update debian/copyright.
  * Document command line options in manpage. (Closes: #525723)
  * debian/rules: drop dh_desktop call.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Mon, 06 Jul 2009 09:36:48 +0200

spout (1.3-1) unstable; urgency=low

  * Initial release. (Closes: #356492)

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Tue, 17 Mar 2009 09:43:03 +0200
